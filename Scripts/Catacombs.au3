#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_UseX64=n
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#include <Array.au3>
#include "Utils\ImageSearch.au3"
#include "Globals.au3"


;;;;;;;;;;;;;;;;
;Start point : in the catacombs facing south. Add rectangle zone if necessary line 104
;Should scan area for possible target and attack
;1080p not full screen but maximized
;;;;;;;;;;;;;;;l

;run script
Global $g_bPaused = False
Global $g_left = False

HotKeySet("{#}","EndApp")
HotKeySet("{PAUSE}", "TogglePause")

Main()

Func Main()
	Local $i = 0
	While 1
		if $i <= 0 Then
			;ResetPosition()
			$i = 20
		EndIf
		Attack()
	WEnd
EndFunc

Func ResetPosition()
	$resultX = 0
	$resultY = 0

	if (_ImageSearchArea("enter.png", _
		1, _
 		0 , _
		0, _
		1920, _
		1080, _
		$resultX, _
		$resultY, _
		100)) Then

		MouseClick("left",$resultX - 30,$resultY + 87,1,0)
		WaitForMove()
	EndIf
EndFunc

Func WaitForMove()
	$resultX = 0
	$resultY = 0
	$PreviousresultX = 0
	$PreviousresultY = 0

	if (_ImageSearchArea("enter.png", _
		1, _
		0 , _
		0, _
		1920, _
		1080, _
		$resultX, _
		$resultY, _
		100)) Then

	EndIf

	Sleep(750)

	Do
		Sleep(750)
		$PreviousresultX = $resultX
		$PreviousresultY = $resultY

		if (_ImageSearchArea("enter.png", _
			1, _
			0 , _
			0, _
			1920, _
			1080, _
			$resultX, _
			$resultY, _
			100)) Then

		EndIf
	Until $PreviousresultX == $resultX AND $PreviousresultY == $resultY


EndFunc

Func Attack()
	$g_left = Not $g_left

	;Make sure its dark in the map to be sure we are not in lumbridge
	CheckIfPlayerIsInDungeon()


	$steps = 20
	;$ATTACK_SEARCH_TORSO_RECT
	;$ATTACK_SEARCH_SPIDER_RECT
	Local $searchRect = $ATTACK_SEARCH_SPIDER_RECT
	$xstep = ($searchRect[2]-$searchRect[0])/$steps
	$ystep = ($searchRect[3]-$searchRect[1])/$steps

	For $i = 0 To $steps Step 1
		$nextStepX = $searchRect[0] + $xstep*$i
		$nextStepY = $searchRect[1] + $ystep*$i

		if Not $g_left Then
			$nextStepX = $searchRect[0] + $xstep*($steps-$i)
		EndIf

		MouseMove($nextStepX,$nextStepY,0)
		Sleep(30)

		if CanAttack($nextStepX,$nextStepY) == 1 Then

			Local $attackHints[1] = ["YellowMob_I.png"]
			if TryAttackMonster($attackHints,$nextStepX,$nextStepY) == 1 Then
				;Wait till fight starts
				Sleep(1500)
				WaitForEndOfFight()
				ExitLoop
			EndIf
		EndIf

	Next

EndFunc


Func CheckIfPlayerIsInDungeon()
	Local $colorCoors = PixelSearch(1870, 465, 1871, 566, 0, 10)
	if @error Then
		EndApp()
	EndIf
EndFunc

Func CanAttack($newX,$newY)
	$resultX = 0
	$resultY = 0

	return _ImageSearchArea("AttackIcon.png", _
		1, _
 		$newX , _
		$newY, _
		$newX + 100, _
		$newY + 100, _
		$resultX, _
		$resultY, _
		100)
EndFunc

Func TryAttackMonster($monsterImage,$currentX,$currentY)
	MouseClick("Right",$currentX,$currentY)
	Sleep(200)
	$resultX = 0
	$resultY = 0
	For $i = 0 To UBound($monsterImage, 1)-1 Step 1
		;Can we find the monster name color?
		if(_ImageSearchArea($monsterImage[$i], 1, $currentX-500 , $currentY-500, $currentX + 300, $currentY + 1000, $resultX, $resultY, 30)) == 1 Then
			;Can we find the green color of the level right next to the monster name? if yes, we can assume its not a player
			Local $colorCoors = PixelSearch($resultX,$resultY-10,$resultX+200,$resultY+10,189189,35)

			if Not @error Then
				MouseClick("Left",$resultX,$resultY,1,0)
				Return 1
			EndIf

		EndIf
	Next
	Return 0
EndFunc

Func WaitForEndOfFight()
	$resultX = 0
	$resultY = 0
	Sleep(100)
	While _ImageSearchArea("TargetLocked.png", 1, 0 , 0, 1920, 1080, $resultX, $resultY, 100)
	WEnd
EndFunc

Func EndApp()
	Exit
EndFunc

Func TogglePause()
    $g_bPaused = Not $g_bPaused
    While $g_bPaused
        Sleep(100)
        ToolTip('Script is "Paused"', 0, 0)
    WEnd
    ToolTip("")
EndFunc
