
Func AddCoord_3D(ByRef $array,$x,$y)

	if $array.Count <= $CurrentVillageIndex Then
		Init_2DArray($array)
	EndIf

	Local $CoordList = ObjCreate('System.Collections.ArrayList')
	$CoordList.Add($x)
	$CoordList.Add($y)

	$array.Item($CurrentVillageIndex).Add($CoordList)
EndFunc

Func AddCoord_2D(ByRef $array,$x,$y)

	if $array.Count <= $CurrentVillageIndex Then
		Init_2DArray($array)
	EndIf

	Local $CoordList = ObjCreate('System.Collections.ArrayList')
	$CoordList.Add($x)
	$CoordList.Add($y)

	$array.Item($CurrentVillageIndex) = $CoordList
EndFunc

Func AddAction_2D(ByRef $array,$action)

	if $array.Count <= $CurrentVillageIndex Then
		Init_2DArray($array)
	EndIf

	$array.Item($CurrentVillageIndex).Add($action)
EndFunc


Func Init_3DArray(ByRef $array)
	Local $arr2 = ObjCreate('System.Collections.ArrayList')
	Local $arr3 = ObjCreate('System.Collections.ArrayList')

	$arr2.Add($arr3)
	$array.Add($arr2)
EndFunc

Func Init_2DArray(ByRef $array)
	Local $arr2 = ObjCreate('System.Collections.ArrayList')

	$array.Add($arr2)
EndFunc