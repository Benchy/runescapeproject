#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_UseX64=n
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#include <Array.au3>
#include "Utils\ImageSearch.au3"
#include "Globals.au3"
#include "Utils\ColorUtil.au3"
#include "Utils\RuneScapeUtils.au3"


;;;;;;;;;;;;;;;;;
;Start point : Left of the red store to steal cake in traverley
;Make sure to be higher camera view facing north (click the compass)
;1080p not full screen but maximized
;
;Should steal cakes until full bank and return to steal.
;;;;;;;;;;;;;;;;;

;run script
Global $g_bPaused = False

HotKeySet("{#}","EndApp")
HotKeySet("{PAUSE}", "TogglePause")

While 1
	Main()
WEnd

Func Main()
	StealCake()
	GoToBankAndBank()
	Sleep(300)

	;;go back to place
	$resultX = 0
	$resultY = 0
	_ImageSearch("Cake_StealingPlace.png",1,$resultX,$resultY,40)
	MouseClick("left",$resultX+15,$resultY+15,1,0)
	WaitForMove("MoveIcon.png")
EndFunc

Func StealCake()
	$resultX = 0
	$resultY = 0
	$i = 0
	While Not IsInventoryFull()
		if(_ImageSearchArea("CakeShop_top.png", 1, 0 , 0, 1920, 1080, $resultX, $resultY, 40)) == 1 Then
			MouseMove($resultX,$resultY+70,0)
			$resultXtemp = 0
			$resultYtemp = 0
			WaitForEndOfFight()
			if(_ImageSearch("CakeShop_StealIcon.png", 1, $resultXtemp, $resultYtemp, 0)) == 1 Then
				MouseClick("left",$resultX,$resultY+70,1,0)
			EndIf


			;When click 1st time wait longer
			if $i == 0 Then
				Sleep(8000)
				$i = $i +1
				ContinueLoop
			Else
				Sleep(4000)
			EndIf

			;Wait till icon disapear, which mean we grabed a cake
			;($findImage,$waitSecs,$resultPosition, ByRef $x, ByRef $y,$tolerance)
			;_WaitForImageSearch("CakeShop_StealIcon.png",10,1,$resultXtemp,$resultYtemp,20)


		EndIf
	WEnd
EndFunc

Func GoToBankAndBank()
	$resultX = 0
	$resultY = 0
	WaitForEndOfFight()

	;go nearer to be sure to see the bank icon
	;_ImageSearch("Cake_Bridge.png",1,$resultX,$resultY,10)
	;MouseClick("left",$resultX,$resultY,1,0)
	;Sleep(2000)
	;WaitForMove("MoveIcon.png")

	;go to bank
	if(_ImageSearch("BankIcon.png", 1, $resultX, $resultY, 10)) == 1 Then
		MouseClick("left",$resultX+15,$resultY+15,1,0)
	EndIf
	Sleep(2000)
	WaitForMove("MoveIcon.png")

	;Search banker
	Local $bankAreaSearch[4] = [748,418,1063,779]
	if ScanImageInArea("BankBanker.png",$bankAreaSearch,$resultX,$resultY,0,20) then
		MouseClick("left",$resultX,$resultY,1,0)
	Else
		print("cant find banker")
		EndApp()
	EndIf

	;empty bag
	Sleep(2000)
	if(_ImageSearch("EmptyBackPack.png", 1, $resultX, $resultY, 10)) == 1 Then
		MouseClick("left",$resultX+5,$resultY+5,1,0)
	EndIf
	Sleep(500)
	Send("{Escape down}")
	Sleep(50)
	Send("{Escape up}")

	Sleep(1500)
	if IsInventoryFull() then
		print("inventory not empty?")
		EndApp()
	EndIf
EndFunc



Func IsInventoryFull()
	return Not CompareColor(PixelGetColor(1785,921), 1247499, 30)
EndFunc

Func WaitForEndOfFight()
	$resultX = 0
	$resultY = 0
	Sleep(100)
	While _ImageSearch("TargetLocked.png", 1, $resultX, $resultY, 100)
	WEnd
EndFunc

Func EndApp()
	Exit
EndFunc

Func TogglePause()
    $g_bPaused = Not $g_bPaused
    While $g_bPaused
        Sleep(100)
        ToolTip('Script is "Paused"', 0, 0)
    WEnd
    ToolTip("")
EndFunc

Func print($msg)
	MsgBox(1,"Error",$msg)
EndFunc
